using Microsoft.AspNetCore.Mvc;

public class ClienteController : Controller
{
    static List<Cliente> lista = new List<Cliente>();
    public ActionResult Index()
    {
        return View(lista);
    }
   
    [HttpGet]

    public ActionResult Create()
    {   
        return View();
    }

    [HttpPost]
    
    public ActionResult Create(Cliente model)
    {   
        lista.Add(model);
        return RedirectToAction("Index");
    }

    public ActionResult Details(int id)
    {   
        foreach (Cliente c in lista)
    
        {
            if(c.ClienteId == id)
                return View(c);
            
            
        }
        return NotFound();
    }

     public ActionResult Delete(int id) {

        foreach(Cliente c in lista) {
            if(c.ClienteId == id)
                lista.Remove(c);
                break;
        }

        return RedirectToAction("Index");
    }

    [HttpPost]
    public ActionResult Update(int id, Cliente model)
    {   
        foreach (Cliente c in lista)
        {
            if(c.ClienteId == id){  
                c.Name = model.Name;
                c.Email = model.Email;
                break;
            }
        }
        return RedirectToAction("Index");
    }
    
    public ActionResult Update(int id) {
        
        foreach(Cliente c in lista) {
            if(c.ClienteId == id)
                return View(c);
        }
        
        return NotFound();
    }
    
    


}