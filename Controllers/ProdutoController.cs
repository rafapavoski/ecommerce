using Microsoft.AspNetCore.Mvc;

public class ProdutoController : Controller
{
    static List<Produto> lista = new List<Produto>();

    // http://locahost:1234/produto/index
    public ActionResult Index()
    {
        // /Views/Produto/Index.cshtml
        return View(lista);
    }

    // http://locahost:1234/produto/create
    [HttpGet]
    public ActionResult Create()
    {
        // /Views/Produto/Create.cshtml
        return View();
    }

    [HttpPost]
    public ActionResult Create(Produto model)
    {
        lista.Add(model);
        return RedirectToAction("Index");
    }

    //  [HttpPost]
    // public ActionResult Create(IFormCollection form)
    // {
    //     string nome = form["nome"];
    //     string preco = form["preco"];

    //     Produto p = new Produto();
    //     p.Nome = nome;
    //     p.Preco = decimal.Parse(preco);

    //     lista.Add(p);

    //     return RedirectToAction("Index");
    // }

    public ActionResult Details(int id)
    {
        foreach(Produto p in lista) {
            if(p.ProdutoId == id)
                return View(p);
        }

        return NotFound();
    }

    public ActionResult Delete(int id) {

        foreach(Produto p in lista) {
            if(p.ProdutoId == id)
                lista.Remove(p);
                break;
        }

        return RedirectToAction("Index");
    }

    public ActionResult Update(int id) {
        
        foreach(Produto p in lista) {
            if(p.ProdutoId == id)
                return View(p);
        }
        
        return NotFound();
    }

    [HttpPost]
    public ActionResult Update(int id, Produto model)
    {
        foreach(Produto p in lista) {
            if(p.ProdutoId == id) {
                p.Nome = model.Nome;
                p.Preco = model.Preco;
                break;
            }
        }

        return RedirectToAction("Index");
    }
}